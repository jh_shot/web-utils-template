---
layout: doc
---

# 安装

## 环境支持

不依赖视图框架。

## 版本

持续开发中

## 使用包管理器

建议您使用包管理器 (NPM, Yarn, PNPM) 安装, 然后您就可以使用打包工具，例如 Vite 和 Webpack

::: code-group

```bash [npm]
npm i @web-utils/library
```

```bash [yarn]
yarn add @web-utils/library
```

```bash [pnpm]
pnpm add @web-utils/library
```

:::

## 浏览器直接引入

暂不支持

## 按需加载

完整路径引用具体工具

```ts
import { AppWorker } from '@web-utils/library'
```
