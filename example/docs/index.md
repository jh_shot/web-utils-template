---
layout: home

title: Web Utils Template
titleTemplate: Web 工具库模板

hero:
  name: Web Utils Template
  text: Web 工具库模板
  tagline: 高频业务工具，仅供内部使用
  actions:
    - theme: brand
      text: 开始
      link: /guide/installation
    - theme: alt
      text: 在 Gitee 上查看
      link: https://gitee.com/jh_shot/web-utils-template

features:
  - icon: 💡
    title: 常用 Web 工具库
    details: 基于vite打包和TypeScript开发
  - icon: 📦
    title: 仅供内部使用
    details: 倾向于内部项目，请勿用于自身项目
  - icon: 🛠️
    title: 按需引入
    details: 直接支持按需引入无需配置任何插件。
---
