import 'element-plus/theme-chalk/dark/css-vars.css'

import DefaultTheme from 'vitepress/theme'
import type { App } from 'vue'

import DemoPreview from '../../components/DemoPreview.vue'

export default {
  ...DefaultTheme,
  enhanceApp({ app }: { app: App }) {
    app.component('DemoPreview', DemoPreview)
  }
}
