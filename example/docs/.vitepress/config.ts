import { join } from 'node:path'

import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import components from 'unplugin-vue-components/vite'
import { defineConfig } from 'vitepress'

export default defineConfig({
  title: 'Web 工具库模板',
  description: 'A VitePress Site',
  base: '/web-utils-template/',
  outDir: '../dist',
  lastUpdated: true,
  head: [
    [
      'link',
      {
        rel: 'icon',
        type: 'image/svg+xml',
        href: '/web-utils-template/vue.svg'
      }
    ]
  ],
  themeConfig: {
    nav: [
      { text: '首页', link: '/' },
      { text: '工具', link: '/guide/installation' }
    ],
    sidebar: [
      {
        text: '基础',
        items: [{ text: '安装', link: '/guide/installation' }]
      },
      {
        text: '工具',
        items: [
          { text: 'PDF导出（A4）', link: '/pages/pdf/' },
          { text: 'Worker', link: '/pages/worker/' }
        ]
      }
    ],
    socialLinks: [
      { icon: 'github', link: 'https://gitee.com/jh_shot/web-utils-template' }
    ],
    search: {
      provider: 'local',
      options: {
        translations: {
          button: {
            buttonText: '查找'
          },
          modal: {
            noResultsText: '没有找到',
            footer: {
              navigateText: '导航',
              selectText: '选择',
              closeText: '关闭'
            }
          }
        }
      }
    },
    docFooter: {
      prev: '上一页',
      next: '下一页'
    },
    outlineTitle: '当前页',
    lastUpdatedText: '最近更新'
  },
  markdown: {
    theme: {
      light: 'material-theme-lighter',
      dark: 'material-theme-darker'
    }
  },
  vite: {
    resolve: {
      alias: [
        {
          find: '@docs',
          replacement: join(__dirname, '../../docs')
        }
      ]
    },
    plugins: [
      // @ts-ignore
      components({
        dts: false,
        dirs: [],
        resolvers: [ElementPlusResolver()]
      })
    ],
    server: {
      host: true,
      port: 5173
    },
    ssr: {
      noExternal: ['element-plus']
    }
  }
})
