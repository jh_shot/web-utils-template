import {
  __commonJS
} from "./chunk-LQ2VYIYD.js";

// browser-external:worker_threads
var require_worker_threads = __commonJS({
  "browser-external:worker_threads"(exports, module) {
    module.exports = Object.create(new Proxy({}, {
      get(_, key) {
        if (key !== "__esModule" && key !== "__proto__" && key !== "constructor" && key !== "splice") {
          console.warn(`Module "worker_threads" has been externalized for browser compatibility. Cannot access "worker_threads.${key}" in client code. See https://vitejs.dev/guide/troubleshooting.html#module-externalized-for-browser-compatibility for more details.`);
        }
      }
    }));
  }
});

// browser-external:os
var require_os = __commonJS({
  "browser-external:os"(exports, module) {
    module.exports = Object.create(new Proxy({}, {
      get(_, key) {
        if (key !== "__esModule" && key !== "__proto__" && key !== "constructor" && key !== "splice") {
          console.warn(`Module "os" has been externalized for browser compatibility. Cannot access "os.${key}" in client code. See https://vitejs.dev/guide/troubleshooting.html#module-externalized-for-browser-compatibility for more details.`);
        }
      }
    }));
  }
});

// browser-external:child_process
var require_child_process = __commonJS({
  "browser-external:child_process"(exports, module) {
    module.exports = Object.create(new Proxy({}, {
      get(_, key) {
        if (key !== "__esModule" && key !== "__proto__" && key !== "constructor" && key !== "splice") {
          console.warn(`Module "child_process" has been externalized for browser compatibility. Cannot access "child_process.${key}" in client code. See https://vitejs.dev/guide/troubleshooting.html#module-externalized-for-browser-compatibility for more details.`);
        }
      }
    }));
  }
});

// ../node_modules/.pnpm/workerpool@9.1.1/node_modules/workerpool/dist/workerpool.js
var require_workerpool = __commonJS({
  "../node_modules/.pnpm/workerpool@9.1.1/node_modules/workerpool/dist/workerpool.js"(exports, module) {
    (function(global, factory) {
      typeof exports === "object" && typeof module !== "undefined" ? factory(exports) : typeof define === "function" && define.amd ? define(["exports"], factory) : (global = typeof globalThis !== "undefined" ? globalThis : global || self, factory(global.workerpool = {}));
    })(exports, function(exports2) {
      "use strict";
      var src = {};
      var environment = { exports: {} };
      (function(module2) {
        var isNode = function(nodeProcess) {
          return typeof nodeProcess !== "undefined" && nodeProcess.versions != null && nodeProcess.versions.node != null && nodeProcess + "" === "[object process]";
        };
        module2.exports.isNode = isNode;
        module2.exports.platform = typeof process !== "undefined" && isNode(process) ? "node" : "browser";
        var worker_threads = module2.exports.platform === "node" && require_worker_threads();
        module2.exports.isMainThread = module2.exports.platform === "node" ? (!worker_threads || worker_threads.isMainThread) && !process.connected : typeof Window !== "undefined";
        module2.exports.cpus = module2.exports.platform === "browser" ? self.navigator.hardwareConcurrency : require_os().cpus().length;
      })(environment);
      var environmentExports = environment.exports;
      var _Promise$1 = {};
      var hasRequired_Promise;
      function require_Promise() {
        if (hasRequired_Promise)
          return _Promise$1;
        hasRequired_Promise = 1;
        function Promise2(handler, parent) {
          var me = this;
          if (!(this instanceof Promise2)) {
            throw new SyntaxError("Constructor must be called with the new operator");
          }
          if (typeof handler !== "function") {
            throw new SyntaxError("Function parameter handler(resolve, reject) missing");
          }
          var _onSuccess = [];
          var _onFail = [];
          this.resolved = false;
          this.rejected = false;
          this.pending = true;
          var _process = function(onSuccess, onFail) {
            _onSuccess.push(onSuccess);
            _onFail.push(onFail);
          };
          this.then = function(onSuccess, onFail) {
            return new Promise2(function(resolve, reject) {
              var s = onSuccess ? _then(onSuccess, resolve, reject) : resolve;
              var f = onFail ? _then(onFail, resolve, reject) : reject;
              _process(s, f);
            }, me);
          };
          var _resolve = function(result) {
            me.resolved = true;
            me.rejected = false;
            me.pending = false;
            _onSuccess.forEach(function(fn) {
              fn(result);
            });
            _process = function(onSuccess, onFail) {
              onSuccess(result);
            };
            _resolve = _reject = function() {
            };
            return me;
          };
          var _reject = function(error) {
            me.resolved = false;
            me.rejected = true;
            me.pending = false;
            _onFail.forEach(function(fn) {
              fn(error);
            });
            _process = function(onSuccess, onFail) {
              onFail(error);
            };
            _resolve = _reject = function() {
            };
            return me;
          };
          this.cancel = function() {
            if (parent) {
              parent.cancel();
            } else {
              _reject(new CancellationError());
            }
            return me;
          };
          this.timeout = function(delay) {
            if (parent) {
              parent.timeout(delay);
            } else {
              var timer = setTimeout(function() {
                _reject(new TimeoutError("Promise timed out after " + delay + " ms"));
              }, delay);
              me.always(function() {
                clearTimeout(timer);
              });
            }
            return me;
          };
          handler(function(result) {
            _resolve(result);
          }, function(error) {
            _reject(error);
          });
        }
        function _then(callback, resolve, reject) {
          return function(result) {
            try {
              var res = callback(result);
              if (res && typeof res.then === "function" && typeof res["catch"] === "function") {
                res.then(resolve, reject);
              } else {
                resolve(res);
              }
            } catch (error) {
              reject(error);
            }
          };
        }
        Promise2.prototype["catch"] = function(onFail) {
          return this.then(null, onFail);
        };
        Promise2.prototype.always = function(fn) {
          return this.then(fn, fn);
        };
        Promise2.all = function(promises) {
          return new Promise2(function(resolve, reject) {
            var remaining = promises.length, results = [];
            if (remaining) {
              promises.forEach(function(p, i) {
                p.then(function(result) {
                  results[i] = result;
                  remaining--;
                  if (remaining == 0) {
                    resolve(results);
                  }
                }, function(error) {
                  remaining = 0;
                  reject(error);
                });
              });
            } else {
              resolve(results);
            }
          });
        };
        Promise2.defer = function() {
          var resolver = {};
          resolver.promise = new Promise2(function(resolve, reject) {
            resolver.resolve = resolve;
            resolver.reject = reject;
          });
          return resolver;
        };
        function CancellationError(message) {
          this.message = message || "promise cancelled";
          this.stack = new Error().stack;
        }
        CancellationError.prototype = new Error();
        CancellationError.prototype.constructor = Error;
        CancellationError.prototype.name = "CancellationError";
        Promise2.CancellationError = CancellationError;
        function TimeoutError(message) {
          this.message = message || "timeout exceeded";
          this.stack = new Error().stack;
        }
        TimeoutError.prototype = new Error();
        TimeoutError.prototype.constructor = Error;
        TimeoutError.prototype.name = "TimeoutError";
        Promise2.TimeoutError = TimeoutError;
        _Promise$1.Promise = Promise2;
        return _Promise$1;
      }
      var WorkerHandler = { exports: {} };
      var validateOptions = {};
      var hasRequiredValidateOptions;
      function requireValidateOptions() {
        if (hasRequiredValidateOptions)
          return validateOptions;
        hasRequiredValidateOptions = 1;
        validateOptions.validateOptions = function validateOptions2(options, allowedOptionNames, objectName) {
          if (!options) {
            return;
          }
          var optionNames = options ? Object.keys(options) : [];
          var unknownOptionName = optionNames.find((optionName) => !allowedOptionNames.includes(optionName));
          if (unknownOptionName) {
            throw new Error('Object "' + objectName + '" contains an unknown option "' + unknownOptionName + '"');
          }
          var illegalOptionName = allowedOptionNames.find((allowedOptionName) => {
            return Object.prototype[allowedOptionName] && !optionNames.includes(allowedOptionName);
          });
          if (illegalOptionName) {
            throw new Error('Object "' + objectName + '" contains an inherited option "' + illegalOptionName + '" which is not defined in the object itself but in its prototype. Only plain objects are allowed. Please remove the option from the prototype or override it with a value "undefined".');
          }
          return options;
        };
        validateOptions.workerOptsNames = ["credentials", "name", "type"];
        validateOptions.forkOptsNames = ["cwd", "detached", "env", "execPath", "execArgv", "gid", "serialization", "signal", "killSignal", "silent", "stdio", "uid", "windowsVerbatimArguments", "timeout"];
        validateOptions.workerThreadOptsNames = ["argv", "env", "eval", "execArgv", "stdin", "stdout", "stderr", "workerData", "trackUnmanagedFds", "transferList", "resourceLimits", "name"];
        return validateOptions;
      }
      var embeddedWorker;
      var hasRequiredEmbeddedWorker;
      function requireEmbeddedWorker() {
        if (hasRequiredEmbeddedWorker)
          return embeddedWorker;
        hasRequiredEmbeddedWorker = 1;
        embeddedWorker = `!function(e,n){"object"==typeof exports&&"undefined"!=typeof module?module.exports=n():"function"==typeof define&&define.amd?define(n):(e="undefined"!=typeof globalThis?globalThis:e||self).worker=n()}(this,(function(){"use strict";function e(e){return e&&e.__esModule&&Object.prototype.hasOwnProperty.call(e,"default")?e.default:e}var n={};var t=function(e,n){this.message=e,this.transfer=n};return function(e){var n=t,r={exit:function(){}};if("undefined"!=typeof self&&"function"==typeof postMessage&&"function"==typeof addEventListener)r.on=function(e,n){addEventListener(e,(function(e){n(e.data)}))},r.send=function(e){postMessage(e)};else{if("undefined"==typeof process)throw new Error("Script must be executed as a worker");var o;try{o=require("worker_threads")}catch(e){if("object"!=typeof e||null===e||"MODULE_NOT_FOUND"!==e.code)throw e}if(o&&null!==o.parentPort){var i=o.parentPort;r.send=i.postMessage.bind(i),r.on=i.on.bind(i),r.exit=process.exit.bind(process)}else r.on=process.on.bind(process),r.send=function(e){process.send(e)},r.on("disconnect",(function(){process.exit(1)})),r.exit=process.exit.bind(process)}function s(e){return Object.getOwnPropertyNames(e).reduce((function(n,t){return Object.defineProperty(n,t,{value:e[t],enumerable:!0})}),{})}function d(e){return e&&"function"==typeof e.then&&"function"==typeof e.catch}r.methods={},r.methods.run=function(e,n){var t=new Function("return ("+e+").apply(null, arguments);");return t.apply(t,n)},r.methods.methods=function(){return Object.keys(r.methods)},r.terminationHandler=void 0,r.cleanupAndExit=function(e){var n=function(){r.exit(e)};if(!r.terminationHandler)return n();var t=r.terminationHandler(e);d(t)?t.then(n,n):n()};var u=null;r.on("message",(function(e){if("__workerpool-terminate__"===e)return r.cleanupAndExit(0);try{var t=r.methods[e.method];if(!t)throw new Error('Unknown method "'+e.method+'"');u=e.id;var o=t.apply(t,e.params);d(o)?o.then((function(t){t instanceof n?r.send({id:e.id,result:t.message,error:null},t.transfer):r.send({id:e.id,result:t,error:null}),u=null})).catch((function(n){r.send({id:e.id,result:null,error:s(n)}),u=null})):(o instanceof n?r.send({id:e.id,result:o.message,error:null},o.transfer):r.send({id:e.id,result:o,error:null}),u=null)}catch(n){r.send({id:e.id,result:null,error:s(n)})}})),r.register=function(e,n){if(e)for(var t in e)e.hasOwnProperty(t)&&(r.methods[t]=e[t]);n&&(r.terminationHandler=n.onTerminate),r.send("ready")},r.emit=function(e){if(u){if(e instanceof n)return void r.send({id:u,isEvent:!0,payload:e.message},e.transfer);r.send({id:u,isEvent:!0,payload:e})}},e.add=r.register,e.emit=r.emit}(n),e(n)}));
//# sourceMappingURL=worker.min.js.map
`;
        return embeddedWorker;
      }
      var hasRequiredWorkerHandler;
      function requireWorkerHandler() {
        if (hasRequiredWorkerHandler)
          return WorkerHandler.exports;
        hasRequiredWorkerHandler = 1;
        var {
          Promise: Promise2
        } = require_Promise();
        var environment2 = environmentExports;
        const {
          validateOptions: validateOptions2,
          forkOptsNames,
          workerThreadOptsNames,
          workerOptsNames
        } = requireValidateOptions();
        var TERMINATE_METHOD_ID = "__workerpool-terminate__";
        function ensureWorkerThreads() {
          var WorkerThreads = tryRequireWorkerThreads();
          if (!WorkerThreads) {
            throw new Error("WorkerPool: workerType = 'thread' is not supported, Node >= 11.7.0 required");
          }
          return WorkerThreads;
        }
        function ensureWebWorker() {
          if (typeof Worker !== "function" && (typeof Worker !== "object" || typeof Worker.prototype.constructor !== "function")) {
            throw new Error("WorkerPool: Web Workers not supported");
          }
        }
        function tryRequireWorkerThreads() {
          try {
            return require_worker_threads();
          } catch (error) {
            if (typeof error === "object" && error !== null && error.code === "MODULE_NOT_FOUND") {
              return null;
            } else {
              throw error;
            }
          }
        }
        function getDefaultWorker() {
          if (environment2.platform === "browser") {
            if (typeof Blob === "undefined") {
              throw new Error("Blob not supported by the browser");
            }
            if (!window.URL || typeof window.URL.createObjectURL !== "function") {
              throw new Error("URL.createObjectURL not supported by the browser");
            }
            var blob = new Blob([requireEmbeddedWorker()], {
              type: "text/javascript"
            });
            return window.URL.createObjectURL(blob);
          } else {
            return __dirname + "/worker.js";
          }
        }
        function setupWorker(script, options) {
          if (options.workerType === "web") {
            ensureWebWorker();
            return setupBrowserWorker(script, options.workerOpts, Worker);
          } else if (options.workerType === "thread") {
            WorkerThreads = ensureWorkerThreads();
            return setupWorkerThreadWorker(script, WorkerThreads, options);
          } else if (options.workerType === "process" || !options.workerType) {
            return setupProcessWorker(script, resolveForkOptions(options), require_child_process());
          } else {
            if (environment2.platform === "browser") {
              ensureWebWorker();
              return setupBrowserWorker(script, options.workerOpts, Worker);
            } else {
              var WorkerThreads = tryRequireWorkerThreads();
              if (WorkerThreads) {
                return setupWorkerThreadWorker(script, WorkerThreads, options);
              } else {
                return setupProcessWorker(script, resolveForkOptions(options), require_child_process());
              }
            }
          }
        }
        function setupBrowserWorker(script, workerOpts, Worker2) {
          validateOptions2(workerOpts, workerOptsNames, "workerOpts");
          var worker2 = new Worker2(script, workerOpts);
          worker2.isBrowserWorker = true;
          worker2.on = function(event, callback) {
            this.addEventListener(event, function(message) {
              callback(message.data);
            });
          };
          worker2.send = function(message, transfer2) {
            this.postMessage(message, transfer2);
          };
          return worker2;
        }
        function setupWorkerThreadWorker(script, WorkerThreads, options) {
          validateOptions2(options == null ? void 0 : options.workerThreadOpts, workerThreadOptsNames, "workerThreadOpts");
          var worker2 = new WorkerThreads.Worker(script, {
            stdout: (options == null ? void 0 : options.emitStdStreams) ?? false,
            // pipe worker.STDOUT to process.STDOUT if not requested
            stderr: (options == null ? void 0 : options.emitStdStreams) ?? false,
            // pipe worker.STDERR to process.STDERR if not requested
            ...options == null ? void 0 : options.workerThreadOpts
          });
          worker2.isWorkerThread = true;
          worker2.send = function(message, transfer2) {
            this.postMessage(message, transfer2);
          };
          worker2.kill = function() {
            this.terminate();
            return true;
          };
          worker2.disconnect = function() {
            this.terminate();
          };
          if (options == null ? void 0 : options.emitStdStreams) {
            worker2.stdout.on("data", (data) => worker2.emit("stdout", data));
            worker2.stderr.on("data", (data) => worker2.emit("stderr", data));
          }
          return worker2;
        }
        function setupProcessWorker(script, options, child_process) {
          validateOptions2(options.forkOpts, forkOptsNames, "forkOpts");
          var worker2 = child_process.fork(script, options.forkArgs, options.forkOpts);
          var send = worker2.send;
          worker2.send = function(message) {
            return send.call(worker2, message);
          };
          if (options.emitStdStreams) {
            worker2.stdout.on("data", (data) => worker2.emit("stdout", data));
            worker2.stderr.on("data", (data) => worker2.emit("stderr", data));
          }
          worker2.isChildProcess = true;
          return worker2;
        }
        function resolveForkOptions(opts) {
          opts = opts || {};
          var processExecArgv = process.execArgv.join(" ");
          var inspectorActive = processExecArgv.indexOf("--inspect") !== -1;
          var debugBrk = processExecArgv.indexOf("--debug-brk") !== -1;
          var execArgv = [];
          if (inspectorActive) {
            execArgv.push("--inspect=" + opts.debugPort);
            if (debugBrk) {
              execArgv.push("--debug-brk");
            }
          }
          process.execArgv.forEach(function(arg) {
            if (arg.indexOf("--max-old-space-size") > -1) {
              execArgv.push(arg);
            }
          });
          return Object.assign({}, opts, {
            forkArgs: opts.forkArgs,
            forkOpts: Object.assign({}, opts.forkOpts, {
              execArgv: (opts.forkOpts && opts.forkOpts.execArgv || []).concat(execArgv),
              stdio: opts.emitStdStreams ? "pipe" : void 0
            })
          });
        }
        function objectToError(obj) {
          var temp = new Error("");
          var props = Object.keys(obj);
          for (var i = 0; i < props.length; i++) {
            temp[props[i]] = obj[props[i]];
          }
          return temp;
        }
        function handleEmittedStdPayload(handler, payload) {
          if (Object.keys(handler.processing).length !== 1) {
            return;
          }
          var task = Object.values(handler.processing)[0];
          if (task.options && typeof task.options.on === "function") {
            task.options.on(payload);
          }
        }
        function WorkerHandler$1(script, _options) {
          var me = this;
          var options = _options || {};
          this.script = script || getDefaultWorker();
          this.worker = setupWorker(this.script, options);
          this.debugPort = options.debugPort;
          this.forkOpts = options.forkOpts;
          this.forkArgs = options.forkArgs;
          this.workerOpts = options.workerOpts;
          this.workerThreadOpts = options.workerThreadOpts;
          this.workerTerminateTimeout = options.workerTerminateTimeout;
          if (!script) {
            this.worker.ready = true;
          }
          this.requestQueue = [];
          this.worker.on("stdout", function(data) {
            handleEmittedStdPayload(me, {
              "stdout": data.toString()
            });
          });
          this.worker.on("stderr", function(data) {
            handleEmittedStdPayload(me, {
              "stderr": data.toString()
            });
          });
          this.worker.on("message", function(response) {
            if (me.terminated) {
              return;
            }
            if (typeof response === "string" && response === "ready") {
              me.worker.ready = true;
              dispatchQueuedRequests();
            } else {
              var id = response.id;
              var task = me.processing[id];
              if (task !== void 0) {
                if (response.isEvent) {
                  if (task.options && typeof task.options.on === "function") {
                    task.options.on(response.payload);
                  }
                } else {
                  delete me.processing[id];
                  if (me.terminating === true) {
                    me.terminate();
                  }
                  if (response.error) {
                    task.resolver.reject(objectToError(response.error));
                  } else {
                    task.resolver.resolve(response.result);
                  }
                }
              }
            }
          });
          function onError(error) {
            me.terminated = true;
            for (var id in me.processing) {
              if (me.processing[id] !== void 0) {
                me.processing[id].resolver.reject(error);
              }
            }
            me.processing = /* @__PURE__ */ Object.create(null);
          }
          function dispatchQueuedRequests() {
            for (const request of me.requestQueue.splice(0)) {
              me.worker.send(request.message, request.transfer);
            }
          }
          var worker2 = this.worker;
          this.worker.on("error", onError);
          this.worker.on("exit", function(exitCode, signalCode) {
            var message = "Workerpool Worker terminated Unexpectedly\n";
            message += "    exitCode: `" + exitCode + "`\n";
            message += "    signalCode: `" + signalCode + "`\n";
            message += "    workerpool.script: `" + me.script + "`\n";
            message += "    spawnArgs: `" + worker2.spawnargs + "`\n";
            message += "    spawnfile: `" + worker2.spawnfile + "`\n";
            message += "    stdout: `" + worker2.stdout + "`\n";
            message += "    stderr: `" + worker2.stderr + "`\n";
            onError(new Error(message));
          });
          this.processing = /* @__PURE__ */ Object.create(null);
          this.terminating = false;
          this.terminated = false;
          this.cleaning = false;
          this.terminationHandler = null;
          this.lastId = 0;
        }
        WorkerHandler$1.prototype.methods = function() {
          return this.exec("methods");
        };
        WorkerHandler$1.prototype.exec = function(method, params, resolver, options) {
          if (!resolver) {
            resolver = Promise2.defer();
          }
          var id = ++this.lastId;
          this.processing[id] = {
            id,
            resolver,
            options
          };
          var request = {
            message: {
              id,
              method,
              params
            },
            transfer: options && options.transfer
          };
          if (this.terminated) {
            resolver.reject(new Error("Worker is terminated"));
          } else if (this.worker.ready) {
            this.worker.send(request.message, request.transfer);
          } else {
            this.requestQueue.push(request);
          }
          var me = this;
          return resolver.promise.catch(function(error) {
            if (error instanceof Promise2.CancellationError || error instanceof Promise2.TimeoutError) {
              delete me.processing[id];
              return me.terminateAndNotify(true).then(function() {
                throw error;
              }, function(err) {
                throw err;
              });
            } else {
              throw error;
            }
          });
        };
        WorkerHandler$1.prototype.busy = function() {
          return this.cleaning || Object.keys(this.processing).length > 0;
        };
        WorkerHandler$1.prototype.terminate = function(force, callback) {
          var me = this;
          if (force) {
            for (var id in this.processing) {
              if (this.processing[id] !== void 0) {
                this.processing[id].resolver.reject(new Error("Worker terminated"));
              }
            }
            this.processing = /* @__PURE__ */ Object.create(null);
          }
          if (typeof callback === "function") {
            this.terminationHandler = callback;
          }
          if (!this.busy()) {
            var cleanup = function(err) {
              me.terminated = true;
              me.cleaning = false;
              if (me.worker != null && me.worker.removeAllListeners) {
                me.worker.removeAllListeners("message");
              }
              me.worker = null;
              me.terminating = false;
              if (me.terminationHandler) {
                me.terminationHandler(err, me);
              } else if (err) {
                throw err;
              }
            };
            if (this.worker) {
              if (typeof this.worker.kill === "function") {
                if (this.worker.killed) {
                  cleanup(new Error("worker already killed!"));
                  return;
                }
                var cleanExitTimeout = setTimeout(function() {
                  if (me.worker) {
                    me.worker.kill();
                  }
                }, this.workerTerminateTimeout);
                this.worker.once("exit", function() {
                  clearTimeout(cleanExitTimeout);
                  if (me.worker) {
                    me.worker.killed = true;
                  }
                  cleanup();
                });
                if (this.worker.ready) {
                  this.worker.send(TERMINATE_METHOD_ID);
                } else {
                  this.requestQueue.push({
                    message: TERMINATE_METHOD_ID
                  });
                }
                this.cleaning = true;
                return;
              } else if (typeof this.worker.terminate === "function") {
                this.worker.terminate();
                this.worker.killed = true;
              } else {
                throw new Error("Failed to terminate worker");
              }
            }
            cleanup();
          } else {
            this.terminating = true;
          }
        };
        WorkerHandler$1.prototype.terminateAndNotify = function(force, timeout) {
          var resolver = Promise2.defer();
          if (timeout) {
            resolver.promise.timeout(timeout);
          }
          this.terminate(force, function(err, worker2) {
            if (err) {
              resolver.reject(err);
            } else {
              resolver.resolve(worker2);
            }
          });
          return resolver.promise;
        };
        WorkerHandler.exports = WorkerHandler$1;
        WorkerHandler.exports._tryRequireWorkerThreads = tryRequireWorkerThreads;
        WorkerHandler.exports._setupProcessWorker = setupProcessWorker;
        WorkerHandler.exports._setupBrowserWorker = setupBrowserWorker;
        WorkerHandler.exports._setupWorkerThreadWorker = setupWorkerThreadWorker;
        WorkerHandler.exports.ensureWorkerThreads = ensureWorkerThreads;
        return WorkerHandler.exports;
      }
      var debugPortAllocator;
      var hasRequiredDebugPortAllocator;
      function requireDebugPortAllocator() {
        if (hasRequiredDebugPortAllocator)
          return debugPortAllocator;
        hasRequiredDebugPortAllocator = 1;
        var MAX_PORTS = 65535;
        debugPortAllocator = DebugPortAllocator;
        function DebugPortAllocator() {
          this.ports = /* @__PURE__ */ Object.create(null);
          this.length = 0;
        }
        DebugPortAllocator.prototype.nextAvailableStartingAt = function(starting) {
          while (this.ports[starting] === true) {
            starting++;
          }
          if (starting >= MAX_PORTS) {
            throw new Error("WorkerPool debug port limit reached: " + starting + ">= " + MAX_PORTS);
          }
          this.ports[starting] = true;
          this.length++;
          return starting;
        };
        DebugPortAllocator.prototype.releasePort = function(port) {
          delete this.ports[port];
          this.length--;
        };
        return debugPortAllocator;
      }
      var Pool_1;
      var hasRequiredPool;
      function requirePool() {
        if (hasRequiredPool)
          return Pool_1;
        hasRequiredPool = 1;
        var {
          Promise: Promise2
        } = require_Promise();
        var WorkerHandler2 = requireWorkerHandler();
        var environment2 = environmentExports;
        var DebugPortAllocator = requireDebugPortAllocator();
        var DEBUG_PORT_ALLOCATOR = new DebugPortAllocator();
        function Pool(script, options) {
          if (typeof script === "string") {
            this.script = script || null;
          } else {
            this.script = null;
            options = script;
          }
          this.workers = [];
          this.tasks = [];
          options = options || {};
          this.forkArgs = Object.freeze(options.forkArgs || []);
          this.forkOpts = Object.freeze(options.forkOpts || {});
          this.workerOpts = Object.freeze(options.workerOpts || {});
          this.workerThreadOpts = Object.freeze(options.workerThreadOpts || {});
          this.debugPortStart = options.debugPortStart || 43210;
          this.nodeWorker = options.nodeWorker;
          this.workerType = options.workerType || options.nodeWorker || "auto";
          this.maxQueueSize = options.maxQueueSize || Infinity;
          this.workerTerminateTimeout = options.workerTerminateTimeout || 1e3;
          this.onCreateWorker = options.onCreateWorker || (() => null);
          this.onTerminateWorker = options.onTerminateWorker || (() => null);
          this.emitStdStreams = options.emitStdStreams || false;
          if (options && "maxWorkers" in options) {
            validateMaxWorkers(options.maxWorkers);
            this.maxWorkers = options.maxWorkers;
          } else {
            this.maxWorkers = Math.max((environment2.cpus || 4) - 1, 1);
          }
          if (options && "minWorkers" in options) {
            if (options.minWorkers === "max") {
              this.minWorkers = this.maxWorkers;
            } else {
              validateMinWorkers(options.minWorkers);
              this.minWorkers = options.minWorkers;
              this.maxWorkers = Math.max(this.minWorkers, this.maxWorkers);
            }
            this._ensureMinWorkers();
          }
          this._boundNext = this._next.bind(this);
          if (this.workerType === "thread") {
            WorkerHandler2.ensureWorkerThreads();
          }
        }
        Pool.prototype.exec = function(method, params, options) {
          if (params && !Array.isArray(params)) {
            throw new TypeError('Array expected as argument "params"');
          }
          if (typeof method === "string") {
            var resolver = Promise2.defer();
            if (this.tasks.length >= this.maxQueueSize) {
              throw new Error("Max queue size of " + this.maxQueueSize + " reached");
            }
            var tasks = this.tasks;
            var task = {
              method,
              params,
              resolver,
              timeout: null,
              options
            };
            tasks.push(task);
            var originalTimeout = resolver.promise.timeout;
            resolver.promise.timeout = function timeout(delay) {
              if (tasks.indexOf(task) !== -1) {
                task.timeout = delay;
                return resolver.promise;
              } else {
                return originalTimeout.call(resolver.promise, delay);
              }
            };
            this._next();
            return resolver.promise;
          } else if (typeof method === "function") {
            return this.exec("run", [String(method), params], options);
          } else {
            throw new TypeError('Function or string expected as argument "method"');
          }
        };
        Pool.prototype.proxy = function() {
          if (arguments.length > 0) {
            throw new Error("No arguments expected");
          }
          var pool2 = this;
          return this.exec("methods").then(function(methods) {
            var proxy = {};
            methods.forEach(function(method) {
              proxy[method] = function() {
                return pool2.exec(method, Array.prototype.slice.call(arguments));
              };
            });
            return proxy;
          });
        };
        Pool.prototype._next = function() {
          if (this.tasks.length > 0) {
            var worker2 = this._getWorker();
            if (worker2) {
              var me = this;
              var task = this.tasks.shift();
              if (task.resolver.promise.pending) {
                var promise = worker2.exec(task.method, task.params, task.resolver, task.options).then(me._boundNext).catch(function() {
                  if (worker2.terminated) {
                    return me._removeWorker(worker2);
                  }
                }).then(function() {
                  me._next();
                });
                if (typeof task.timeout === "number") {
                  promise.timeout(task.timeout);
                }
              } else {
                me._next();
              }
            }
          }
        };
        Pool.prototype._getWorker = function() {
          var workers = this.workers;
          for (var i = 0; i < workers.length; i++) {
            var worker2 = workers[i];
            if (worker2.busy() === false) {
              return worker2;
            }
          }
          if (workers.length < this.maxWorkers) {
            worker2 = this._createWorkerHandler();
            workers.push(worker2);
            return worker2;
          }
          return null;
        };
        Pool.prototype._removeWorker = function(worker2) {
          var me = this;
          DEBUG_PORT_ALLOCATOR.releasePort(worker2.debugPort);
          this._removeWorkerFromList(worker2);
          this._ensureMinWorkers();
          return new Promise2(function(resolve, reject) {
            worker2.terminate(false, function(err) {
              me.onTerminateWorker({
                forkArgs: worker2.forkArgs,
                forkOpts: worker2.forkOpts,
                workerThreadOpts: worker2.workerThreadOpts,
                script: worker2.script
              });
              if (err) {
                reject(err);
              } else {
                resolve(worker2);
              }
            });
          });
        };
        Pool.prototype._removeWorkerFromList = function(worker2) {
          var index = this.workers.indexOf(worker2);
          if (index !== -1) {
            this.workers.splice(index, 1);
          }
        };
        Pool.prototype.terminate = function(force, timeout) {
          var me = this;
          this.tasks.forEach(function(task) {
            task.resolver.reject(new Error("Pool terminated"));
          });
          this.tasks.length = 0;
          var f = function(worker2) {
            DEBUG_PORT_ALLOCATOR.releasePort(worker2.debugPort);
            this._removeWorkerFromList(worker2);
          };
          var removeWorker = f.bind(this);
          var promises = [];
          var workers = this.workers.slice();
          workers.forEach(function(worker2) {
            var termPromise = worker2.terminateAndNotify(force, timeout).then(removeWorker).always(function() {
              me.onTerminateWorker({
                forkArgs: worker2.forkArgs,
                forkOpts: worker2.forkOpts,
                workerThreadOpts: worker2.workerThreadOpts,
                script: worker2.script
              });
            });
            promises.push(termPromise);
          });
          return Promise2.all(promises);
        };
        Pool.prototype.stats = function() {
          var totalWorkers = this.workers.length;
          var busyWorkers = this.workers.filter(function(worker2) {
            return worker2.busy();
          }).length;
          return {
            totalWorkers,
            busyWorkers,
            idleWorkers: totalWorkers - busyWorkers,
            pendingTasks: this.tasks.length,
            activeTasks: busyWorkers
          };
        };
        Pool.prototype._ensureMinWorkers = function() {
          if (this.minWorkers) {
            for (var i = this.workers.length; i < this.minWorkers; i++) {
              this.workers.push(this._createWorkerHandler());
            }
          }
        };
        Pool.prototype._createWorkerHandler = function() {
          const overriddenParams = this.onCreateWorker({
            forkArgs: this.forkArgs,
            forkOpts: this.forkOpts,
            workerOpts: this.workerOpts,
            workerThreadOpts: this.workerThreadOpts,
            script: this.script
          }) || {};
          return new WorkerHandler2(overriddenParams.script || this.script, {
            forkArgs: overriddenParams.forkArgs || this.forkArgs,
            forkOpts: overriddenParams.forkOpts || this.forkOpts,
            workerOpts: overriddenParams.workerOpts || this.workerOpts,
            workerThreadOpts: overriddenParams.workerThreadOpts || this.workerThreadOpts,
            debugPort: DEBUG_PORT_ALLOCATOR.nextAvailableStartingAt(this.debugPortStart),
            workerType: this.workerType,
            workerTerminateTimeout: this.workerTerminateTimeout,
            emitStdStreams: this.emitStdStreams
          });
        };
        function validateMaxWorkers(maxWorkers) {
          if (!isNumber(maxWorkers) || !isInteger(maxWorkers) || maxWorkers < 1) {
            throw new TypeError("Option maxWorkers must be an integer number >= 1");
          }
        }
        function validateMinWorkers(minWorkers) {
          if (!isNumber(minWorkers) || !isInteger(minWorkers) || minWorkers < 0) {
            throw new TypeError("Option minWorkers must be an integer number >= 0");
          }
        }
        function isNumber(value) {
          return typeof value === "number";
        }
        function isInteger(value) {
          return Math.round(value) == value;
        }
        Pool_1 = Pool;
        return Pool_1;
      }
      var worker$1 = {};
      var transfer;
      var hasRequiredTransfer;
      function requireTransfer() {
        if (hasRequiredTransfer)
          return transfer;
        hasRequiredTransfer = 1;
        function Transfer2(message, transfer2) {
          this.message = message;
          this.transfer = transfer2;
        }
        transfer = Transfer2;
        return transfer;
      }
      var hasRequiredWorker;
      function requireWorker() {
        if (hasRequiredWorker)
          return worker$1;
        hasRequiredWorker = 1;
        (function(exports3) {
          var Transfer2 = requireTransfer();
          var TERMINATE_METHOD_ID = "__workerpool-terminate__";
          var worker2 = {
            exit: function() {
            }
          };
          if (typeof self !== "undefined" && typeof postMessage === "function" && typeof addEventListener === "function") {
            worker2.on = function(event, callback) {
              addEventListener(event, function(message) {
                callback(message.data);
              });
            };
            worker2.send = function(message) {
              postMessage(message);
            };
          } else if (typeof process !== "undefined") {
            var WorkerThreads;
            try {
              WorkerThreads = require_worker_threads();
            } catch (error) {
              if (typeof error === "object" && error !== null && error.code === "MODULE_NOT_FOUND")
                ;
              else {
                throw error;
              }
            }
            if (WorkerThreads && /* if there is a parentPort, we are in a WorkerThread */
            WorkerThreads.parentPort !== null) {
              var parentPort = WorkerThreads.parentPort;
              worker2.send = parentPort.postMessage.bind(parentPort);
              worker2.on = parentPort.on.bind(parentPort);
              worker2.exit = process.exit.bind(process);
            } else {
              worker2.on = process.on.bind(process);
              worker2.send = function(message) {
                process.send(message);
              };
              worker2.on("disconnect", function() {
                process.exit(1);
              });
              worker2.exit = process.exit.bind(process);
            }
          } else {
            throw new Error("Script must be executed as a worker");
          }
          function convertError(error) {
            return Object.getOwnPropertyNames(error).reduce(function(product, name) {
              return Object.defineProperty(product, name, {
                value: error[name],
                enumerable: true
              });
            }, {});
          }
          function isPromise(value) {
            return value && typeof value.then === "function" && typeof value.catch === "function";
          }
          worker2.methods = {};
          worker2.methods.run = function run(fn, args) {
            var f = new Function("return (" + fn + ").apply(null, arguments);");
            return f.apply(f, args);
          };
          worker2.methods.methods = function methods() {
            return Object.keys(worker2.methods);
          };
          worker2.terminationHandler = void 0;
          worker2.cleanupAndExit = function(code) {
            var _exit = function() {
              worker2.exit(code);
            };
            if (!worker2.terminationHandler) {
              return _exit();
            }
            var result = worker2.terminationHandler(code);
            if (isPromise(result)) {
              result.then(_exit, _exit);
            } else {
              _exit();
            }
          };
          var currentRequestId = null;
          worker2.on("message", function(request) {
            if (request === TERMINATE_METHOD_ID) {
              return worker2.cleanupAndExit(0);
            }
            try {
              var method = worker2.methods[request.method];
              if (method) {
                currentRequestId = request.id;
                var result = method.apply(method, request.params);
                if (isPromise(result)) {
                  result.then(function(result2) {
                    if (result2 instanceof Transfer2) {
                      worker2.send({
                        id: request.id,
                        result: result2.message,
                        error: null
                      }, result2.transfer);
                    } else {
                      worker2.send({
                        id: request.id,
                        result: result2,
                        error: null
                      });
                    }
                    currentRequestId = null;
                  }).catch(function(err) {
                    worker2.send({
                      id: request.id,
                      result: null,
                      error: convertError(err)
                    });
                    currentRequestId = null;
                  });
                } else {
                  if (result instanceof Transfer2) {
                    worker2.send({
                      id: request.id,
                      result: result.message,
                      error: null
                    }, result.transfer);
                  } else {
                    worker2.send({
                      id: request.id,
                      result,
                      error: null
                    });
                  }
                  currentRequestId = null;
                }
              } else {
                throw new Error('Unknown method "' + request.method + '"');
              }
            } catch (err) {
              worker2.send({
                id: request.id,
                result: null,
                error: convertError(err)
              });
            }
          });
          worker2.register = function(methods, options) {
            if (methods) {
              for (var name in methods) {
                if (methods.hasOwnProperty(name)) {
                  worker2.methods[name] = methods[name];
                }
              }
            }
            if (options) {
              worker2.terminationHandler = options.onTerminate;
            }
            worker2.send("ready");
          };
          worker2.emit = function(payload) {
            if (currentRequestId) {
              if (payload instanceof Transfer2) {
                worker2.send({
                  id: currentRequestId,
                  isEvent: true,
                  payload: payload.message
                }, payload.transfer);
                return;
              }
              worker2.send({
                id: currentRequestId,
                isEvent: true,
                payload
              });
            }
          };
          {
            exports3.add = worker2.register;
            exports3.emit = worker2.emit;
          }
        })(worker$1);
        return worker$1;
      }
      const {
        platform,
        isMainThread,
        cpus
      } = environmentExports;
      function pool(script, options) {
        var Pool = requirePool();
        return new Pool(script, options);
      }
      var pool_1 = src.pool = pool;
      function worker(methods, options) {
        var worker2 = requireWorker();
        worker2.add(methods, options);
      }
      var worker_1 = src.worker = worker;
      function workerEmit(payload) {
        var worker2 = requireWorker();
        worker2.emit(payload);
      }
      var workerEmit_1 = src.workerEmit = workerEmit;
      const {
        Promise: Promise$1
      } = require_Promise();
      var _Promise = src.Promise = Promise$1;
      var Transfer = src.Transfer = requireTransfer();
      var platform_1 = src.platform = platform;
      var isMainThread_1 = src.isMainThread = isMainThread;
      var cpus_1 = src.cpus = cpus;
      exports2.Promise = _Promise;
      exports2.Transfer = Transfer;
      exports2.cpus = cpus_1;
      exports2.default = src;
      exports2.isMainThread = isMainThread_1;
      exports2.platform = platform_1;
      exports2.pool = pool_1;
      exports2.worker = worker_1;
      exports2.workerEmit = workerEmit_1;
      Object.defineProperty(exports2, "__esModule", { value: true });
    });
  }
});
export default require_workerpool();
/*! Bundled license information:

workerpool/dist/workerpool.js:
  (**
   * workerpool.js
   * https://github.com/josdejong/workerpool
   *
   * Offload tasks to a pool of workers on node.js and in the browser.
   *
   * @version 9.1.1
   * @date    2024-04-06
   *
   * @license
   * Copyright (C) 2014-2022 Jos de Jong <wjosdejong@gmail.com>
   *
   * Licensed under the Apache License, Version 2.0 (the "License"); you may not
   * use this file except in compliance with the License. You may obtain a copy
   * of the License at
   *
   * http://www.apache.org/licenses/LICENSE-2.0
   *
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
   * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
   * License for the specific language governing permissions and limitations under
   * the License.
   *)
*/
//# sourceMappingURL=workerpool.js.map
