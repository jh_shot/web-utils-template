---
layout: doc
---

<script setup>
import DemoBasic from './DemoBasic.vue'
import DemoDepts from './DemoDepts.vue'
</script>

# Worker

## 安装依赖

```bash
npm i workerpool
npm i @types/workerpool -D
```

## 使用示例

### 基础用法

使用 `AppWorker` 的静态属性 `pool` 执行一些简单的方法。

<demo-preview>
  <DemoBasic />
</demo-preview>

::: details 点击查看
<<< ./DemoBasic.vue
:::

### 引入依赖

实例化 `AppWorker` 后调用 `run` 方法，适用需要引入第三方依赖的场景，例如：lodash。

<demo-preview>
  <DemoDepts />
</demo-preview>

::: details 点击查看
<<< ./DemoDepts.vue
:::

## 类型定义

::: details 点击查看
<<< @/../../library/dist/worker/worker.d.ts
:::
