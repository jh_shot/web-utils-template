import workerpool from 'workerpool'

import depts from './depts'

function runWithDepts(fn: any, ...args: any) {
  try {
    const f = new Function('return (' + fn + ').apply(null, arguments);')
    return f.apply(f, [depts].concat(args))
  } catch (e) {
    console.error(e)
    throw e
  }
}

workerpool.worker({
  runWithDepts
})
