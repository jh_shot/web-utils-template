import { memoize } from 'lodash-unified'

const depts = {
  memoize
}

export default depts
