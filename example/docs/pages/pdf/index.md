---
layout: doc
---

<script setup>
import DemoMulti from './DemoMulti.vue'
import DemoSingle from './DemoSingle.vue'
</script>

# PDF导出（A4）

## 安装依赖

```bash
npm i html2canvas jspdf
```

## 使用示例
  
### 单节点导出

<demo-preview br>
  <DemoSingle />
</demo-preview>

::: details 点击查看
<<< ./DemoSingle.vue
:::

### 多节点导出

<demo-preview br>
  <DemoMulti />
</demo-preview>

::: details 点击查看
<<< ./DemoMulti.vue
:::

## 类型定义

::: details 点击查看
<<< @/../../library/dist/pdf/pdf.d.ts
:::
