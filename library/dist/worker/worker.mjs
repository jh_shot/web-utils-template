import t from "workerpool";
class a {
  constructor(r) {
    this.MAX_WORKER = 8, this.workerPath = r.workerPath, this.workerName = r.workerName;
  }
  /**
   * 静态线程池
   * @description 用来执行一些简单任务
   */
  static get pool() {
    return t.pool();
  }
  /**
   * 执行方法
   * @param fn 方法函数
   * @param args 方法参数
   * @returns 执行结果
   */
  run(r, ...o) {
    return this.pool || (this.patchWorker(), this.pool = t.pool(this.workerPath, {
      maxWorkers: this.MAX_WORKER
    })), this.pool.exec(this.workerName, [String(r)].concat(o));
  }
  patchWorker() {
    if (globalThis.Worker && !globalThis.Worker.patched) {
      const o = class o extends Worker {
        constructor(e, s) {
          super(e, Object.assign({}, s, { type: "module" }));
        }
      };
      o.patched = !0;
      let r = o;
      globalThis.Worker = r;
    }
  }
}
export {
  a as AppWorker
};
