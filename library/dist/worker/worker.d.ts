import { Pool, default as workerpool } from 'workerpool';

interface IOptions {
    /**
     * worker 文件相对路径
     * @example "new URL('./runWithDepts.ts?worker_file', import.meta.url).href"
     */
    workerPath: string;
    /**
     * worker 方法名称
     * @description 与传入 `workerpool.worker({ workerName })` 参数相同
     * @example "runWithDepts"
     */
    workerName: string;
}
export declare class AppWorker {
    /**
     * 最大线程数
     * @description 与 CPU 有关
     */
    MAX_WORKER: number;
    /**
     * 线程池
     */
    pool: Pool | undefined;
    /**
     * worker 文件路径
     */
    workerPath: string;
    /**
     * worker 方法名称
     */
    workerName: string;
    constructor(options: IOptions);
    /**
     * 静态线程池
     * @description 用来执行一些简单任务
     */
    static get pool(): import("workerpool/types/Pool");
    /**
     * 执行方法
     * @param fn 方法函数
     * @param args 方法参数
     * @returns 执行结果
     */
    run<T extends any[], R>(fn: (depts: any, ...args: T) => Promise<R> | R, ...args: T): workerpool.Promise<any, Error>;
    private patchWorker;
}
export {};
