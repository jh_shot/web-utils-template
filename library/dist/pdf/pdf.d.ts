/**
 * 打印码说明
 * * 第一位为打印方向, 即第一打印方向
 * * 第二位也为打印方向, 但可省略, 不传时, 仅按一个方向打印
 */
declare const exportMode: {
    "h-v": string;
    h: string;
    v: string;
    "v-h": string;
};
interface ConfigTemp {
    /**
     * 导出文件名
     * @default '文件.pdf'
     */
    fileName?: string;
    /**
     * 打印方向
     * @description landscape: 横向, portrait: 纵向;
     * @default 'portrait'
     */
    orientation?: 'portrait' | 'p' | 'l' | 'landscape';
    /**
     * 多页打印控制
     * @default 'v'
     */
    multiMode?: keyof typeof exportMode;
    /**
     * 放大倍数
     * @description 该参数谨慎使用, 容易导致内存占用过高, 拖慢速度, 甚至导致失败
     * @default 1
     */
    scale?: number;
    /**
     * 切分大小
     * @description 当调用 pdfExportMulti 时生效
     * @default 10
     */
    chunkSize?: number;
}
/**
 * PDF导出 - 单节点
 * @param dom DOM 节点
 * @param config 导出配置
 * @param callback 回调方法
 */
export declare function pdfExportSingle(dom: HTMLElement, config?: ConfigTemp, callback?: (...args: any[]) => void): Promise<void>;
/**
 * PDF导出 - 多节点
 * @description 适用于大文件导出
 * @param doms DOM 节点数组
 * @param config 导出配置
 * @param callback 回调方法
 */
export declare function pdfExportMulti(doms: HTMLElement[], config?: ConfigTemp, callback?: (...args: any[]) => void): Promise<void>;
export {};
