import C from "html2canvas";
import M from "jspdf";
const d = {
  "h-v": "h-v",
  //  打印顺序: 先横向, 再垂直
  h: "h",
  //  仅横向
  v: "v",
  //  仅垂直 [默认]
  "v-h": "v-h"
  //  先垂直, 再横向
}, f = {
  portrait: [595.28, 841.89],
  // 纵向比
  landscape: [841.89, 595.28]
  // 横线比
}, x = {
  fileName: "文件.pdf",
  orientation: "portrait",
  multiMode: d.v,
  scale: 1,
  chunkSize: 10
};
function y(a, e) {
  return a.reduce((h, r, t) => t % e ? h : [...h, h[h.length - 1].concat(r)], [
    []
  ]).slice(1);
}
function S(a) {
  if (!a.fileName)
    return "fileName";
  if (!a.chunkSize)
    return "chunkSize";
  if (!a.multiMode)
    return "multiMode";
  if (!a.orientation)
    return "orientation";
  if (!a.scale)
    return "scale";
}
function w(a, e, h, r, t) {
  const o = { x: 0, y: 0 }, { docImage: l, contentSize: u, unExportSize: n, pageSize: s } = h;
  if (e === d.v) {
    const i = r * 1, c = i / u.w * u.h;
    for (console.log("contentSize: ", u), console.log("page: ", s), console.log("imgW, imgH, a4W, a4H: ", i, c, r, t); Math.floor(n.h) > 0; )
      a.addImage(l, "JPEG", o.x, o.y, i, c), console.log("unExport.h: ", n.h), console.log("exportOffset: ", o), n.h = n.h - s.h / 1, o.y = o.y - t, Math.floor(n.h) > 0 && a.addPage();
    console.log("unExport.h: ", n.h);
  }
  if (e === d.h) {
    const i = r * 1, c = i / u.w * u.h;
    for (console.log("contentSize: ", u), console.log("page: ", s), console.log("imgW, imgH, a4W, a4H: ", i, c, r, t); Math.floor(n.h) > 0; )
      a.addImage(l, "JPEG", o.x, o.y, i, c), console.log("unExport.h: ", n.h), n.h = n.h - s.h / 1, o.y = o.y - t, Math.floor(n.h) > 0 && a.addPage();
    console.log("unExport.h: ", n.h);
  }
  if (e === d["h-v"]) {
    const c = r * 2, p = c / u.w * u.h;
    console.log("imgW, imgH, a4W, a4H: ", c, p, r, t), console.log("page: ", s);
    for (let v = 0; v < 3; v++)
      for (let g = 0; g < 2; g++)
        a.addImage(
          l,
          "JPEG",
          s.w / 2,
          s.h / 3,
          r,
          p
        ), a.addPage();
    console.log("unExport.h: ", n.h);
  }
}
async function P(a, e = x, h = (...r) => {
}) {
  e = { ...x, ...e };
  const r = S(e);
  if (r) {
    console.error(`PDF导出失败：${r} 值不正确`, e);
    return;
  }
  const [t, o] = f[e.orientation], l = new M({
    orientation: e.orientation,
    unit: "pt",
    format: "a4"
  }), u = await C(a, {
    scale: e.scale,
    allowTaint: !0
  }), n = a.getBoundingClientRect(), s = {
    w: n.width * e.scale,
    h: n.height * e.scale
  }, m = u.toDataURL("image/jpeg", 1), i = {
    contentSize: s,
    unExportSize: {
      w: s.w,
      h: s.h
    },
    pageSize: {
      w: s.h / o * t,
      h: s.w / t * o
    },
    docImage: m
  };
  switch (e.multiMode) {
    case d.v:
      w(l, e.multiMode, i, t, o);
      break;
    case d.h:
      w(l, e.multiMode, i, t, o);
      break;
    case d["v-h"]:
      w(l, e.multiMode, i, t, o);
      break;
    case d["h-v"]:
      w(l, e.multiMode, i, t, o);
      break;
  }
  if (l.save(e.fileName), typeof h == "function") {
    const c = setTimeout(() => {
      h(), c && clearTimeout(c);
    }, 300);
  }
}
async function b(a, e = x, h = (...r) => {
}) {
  e = { ...x, ...e };
  const r = S(e);
  if (r) {
    console.error(`PDF导出失败：${r} 值不正确`, e);
    return;
  }
  const [t, o] = f[e.orientation], l = new M({
    orientation: e.orientation,
    unit: "pt",
    format: "a4"
  }), u = y(a, e.chunkSize);
  for (const [n, s] of u.entries()) {
    const m = document.createElement("div");
    m.style.position = "absolute", m.style.left = "-9999px", s.forEach((z) => {
      m.append(z.cloneNode(!0));
    }), document.body.append(m);
    const i = await C(m, {
      scale: e.scale,
      allowTaint: !0
    }), c = m.getBoundingClientRect(), p = {
      w: c.width * e.scale,
      h: c.height * e.scale
    }, v = i.toDataURL("image/jpeg", 1), g = {
      contentSize: p,
      unExportSize: {
        w: p.w,
        h: p.h
      },
      pageSize: {
        w: p.h / o * t,
        h: p.w / t * o
      },
      docImage: v
    };
    switch (e.multiMode) {
      case d.v:
        w(l, e.multiMode, g, t, o);
        break;
      case d.h:
        w(l, e.multiMode, g, t, o);
        break;
      case d["v-h"]:
        w(l, e.multiMode, g, t, o);
        break;
      case d["h-v"]:
        w(l, e.multiMode, g, t, o);
        break;
    }
    n !== u.length - 1 && l.addPage(), document.body.removeChild(m);
  }
  if (l.save(e.fileName), typeof h == "function") {
    const n = setTimeout(() => {
      h(), n && clearTimeout(n);
    }, 300);
  }
}
export {
  b as pdfExportMulti,
  P as pdfExportSingle
};
