import { AppWorker } from './worker'

describe('AppWorker', () => {
  it('Basic', async () => {
    function fibonacci(n: number): number {
      if (n < 0) throw new Error('输入的数字不能小于0')
      if (n == 1 || n == 2) {
        return 1
      } else {
        return fibonacci(n - 1) + fibonacci(n - 2)
      }
    }
    const res = await AppWorker.pool.exec(fibonacci, [10])

    expect(res).toBe(55)
  })
})
